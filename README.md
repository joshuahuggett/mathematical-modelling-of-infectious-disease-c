# Project Overview
The purpose of this project is to build a realistic simulation of the spread of infectious diseases. It is possible to model disease propagation statistically, but this project is an explicit simulation where we maintain the explicit description of all the people in the population and track for each of them their status. In this simulation, a person can be sick, susceptible, recovered, and vaccinated. When a person is sick, they carry the disease and can infect other people. When a person is susceptible, they can be infected. When a person is recovered, they have been sick but no longer carry the disease, and can not be infected for a second time. When a person is vaccinated, they do not carry the disease and can not be infected.

A person is modeled as a class with the methods status_string(), update(), infect(), and is_stable(). Status_string() returns a description of the person's state. Update() updates the person's status to the next day. Infect() infects a person with the disease to run for a a specified duration.  Is_stable() returns a bool indicating whether the person has been sick and is recovered. 

A population is modeled as a class with a vector containing people, constructor population(), and methods random_infect(), count_infected(), and update(). Population() creates a population with a specified size. Random_infect() infects a random person in the population.  Count_infected() counts how many people in the population are infected. Update() updates the population status to the next day. 

Contagion is incorporated by P, the probability of disease transmission upon contact. Inoculation is incorporated by V, the proportion of people that have been vaccinated, chosen randomly. To make this simulation realistic, we let every sick person come into contact with a fixed number of random people every day. The simulation was run for a constant population size of 100,000 and the following parameters. 

# Results
![](images/3.png = 250x250)
![](images/6.png = 250x250)

