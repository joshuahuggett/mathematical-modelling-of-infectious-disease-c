#include <iostream>
#include <cmath>
#include <vector>
#include <algorithm>

using std::cout;
using std::cin;
using std::endl;
using std::vector;
using std::string;
using std::to_string;

class person
        {
        private:
        int status;
        string status_string;
        bool b;

        public:
        person()
        {
        status=0;
        }

        string status_description()
        {
        if (status==0)
        {
        status_string="healthy";
        }       
        if (status==-1)
        {
        status_string="recovered";
        }
        if (status==-2)
        {
        status_string="vaccinated";
        }
        if (status>0)
        {
        status_string= "sick (" + to_string(status) + " days to go before recovery)";
		}
        return status_string;
        }

        void update()
        {
        if(status>0)
        {
        status=status-1;
                if(status==0)
                {
                status=-1;
                }
        }
        }

        void infect(int x)
        {
        if (status==0)
        {
        status=x;
        }
        }

        bool is_stable()
        {
        if (status==-1)
        {
        b=true;
        }
        else
        {
        b=false;
        }
		
        return b;
        }
		int get_status()
        {
        return status;
        }

        void vaccinate()
        {
        status=-2;
        }
        };

class population
        {
        private:
        int size, infected_number, days, interactions;
        vector<person> population_vector;
        float probability;

        public:
        population(int x)
        {
        size=x;
        for (int i=0; i<size; i++)
        {
        population_vector.push_back(person());
        }
        }

        void random_infect()
        {
        int r;
        for (int i=0;;i++)
        {
        r=rand()%size;
                if (population_vector[r].get_status()==0)
                {
                population_vector[r].infect(days);
                break;
                }
        }
        }

        void neighbor_infect(int x)
        {
        population_vector[x].infect(days);
        }

        void interaction_infect(int x)
        {
        population_vector[x].infect(days);
        }
        int count_infected()
        {
        infected_number=0;
        for (int j=0; j<size; j++)
        {
                if (population_vector[j].get_status()>0)
                {
                infected_number=infected_number+1;
                }
        }
        return infected_number;
        }

        void update()
        {
        for (int k=0; k<size; k++)
        {
        population_vector[k].update();
        }
        }

        int get_status2(int x)
        {
        return population_vector[x].get_status();
        }

        void set_probability(float x)
        {
        probability=x;
        }

        float get_probability()
        {
        return probability;
        }

        void set_days(int x)
        {
        days=x;
        }
		
 		int get_days()
        {
        return days;
        }

        void random_vaccinate(float x)
        {
        int y,r;
        y=size*x;
        for (int i=0; i<y;)
        {
        r=rand()%size;
                if (population_vector[r].get_status()==0)
                {
                population_vector[r].vaccinate();
                i++;
                }
        }
        }

        void set_interactions(int x)
        {
        interactions=x;
        }

        int get_interactions()
        {
        return interactions;
        }
        };
		
int main()
        {
        srand((unsigned)time(0));
        int s, d, i;
        float p,v;
        cout << "Size of population?" << endl;
        cin >> s;
        cout << "Probability of disease transmission upon contact? (between 0 and 1)" << endl;
        cin >> p;
        cout << "Proportion of people that have been vaccinated? (between 0 and 1)" << endl;
        cin >> v;
        cout << "Days the disease lasts?" << endl;
        cin >> d;
        cout << "Number of people that a person interacts with?" << endl;
        cin >> i;
        population austin=population(s);
        austin.set_probability(p);
        austin.random_vaccinate(v);
        austin.set_days(d);
        austin.set_interactions(i);
        austin.random_infect();

		for (int j=1; ; j++)
        {
        cout << "On day " << j << ", the number of infected is " << austin.count_infected() << ". State of popular: " ;
                for (int k=0; k<s; k++)
                {
                        if (austin.get_status2(k)==0)
                        {
                        cout << " ? ";
                        }
                        else if (austin.get_status2(k)==-1)
                        {
                        cout << " - ";
                        }
                        else if (austin.get_status2(k)==-2)
                        {
                        cout << " ! ";
                        }
                        else if (austin.get_status2(k)>0)
                        {
                        cout << " + ";
                        }
                }
        cout << endl;

				if (austin.count_infected()==0)
                {
                cout << "The disease ran its course by step " << j << endl;
                break;
                }

		austin.update();
		
				for (int l=0; l<s; l++)
                {
                float bad_luck=(float)rand()/RAND_MAX;
                vector <int> interaction_vector;

                        for (int m=0; m<austin.get_interactions(); )
                        {
                        int r=rand()%s;
                        int c=count(interaction_vector.begin(), interaction_vector.end(), r);
                                if (c==0)
                                {
                                interaction_vector.push_back(r);
                                m++;
                                }
                        }

                        for (int n=0; n<austin.get_interactions(); n++)
                        {
                                if (austin.get_status2(interaction_vector[n])>0 && austin.get_status2(interaction_vector[n])<austin.get_days() && austin.get_status2(l)==0 && bad_luck<austin.get_probability())
                                {
                                austin.interaction_infect(l);
                                }
                        }
                }

        }
        }